import { createServer, bindEagerlyTo, bindTo } from '@marblejs/core';
import { listener } from './api/http.listener';
import { config } from './config';
import { SequelizeToken, SequelizeReader } from './db';
import { AuthClientToken, authClient } from './microservice/client/auth.client';
import { UserServiceToken, UserServiceReader } from './services/user.service';
import {
  FriendsServiceToken,
  FriendsServiceReader,
} from './services/friends.service';

export const server = createServer({
  port: config.port,
  hostname: config.hostname,
  listener,
  dependencies: [
    bindEagerlyTo(SequelizeToken)(SequelizeReader),
    bindEagerlyTo(AuthClientToken)(authClient),
    bindTo(UserServiceToken)(UserServiceReader),
    bindTo(FriendsServiceToken)(FriendsServiceReader),
  ],
});
