import dotenv from 'dotenv';
import { sequelizeConfig } from './sequelizeConfig';

dotenv.config();

const env = process.env.ENV || 'development';

export const config = {
  port: Number(process.env.PORT) || 3333,
  env: env,
  hostname: process.env.hostName || '127.0.0.1',
  db: sequelizeConfig[env],
};
