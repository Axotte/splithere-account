export const sequelizeConfig = {
  development: {
    database: 'splithere_db',
    username: 'splithere',
    password: 'test123',
    port: 5432,
    host: 'localhost',
    dialect: 'postgres',
    pool: {
      min: 0,
      max: 5,
      acquire: 30000,
      idle: 10000,
    },
  },
  test: {
    name: 'splithere-db',
    user: 'user',
    password: 'test123',
    dialect: 'sqlite',
    storage: ':memory:',
  },
  production: {},
};
