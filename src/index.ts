import { IO } from 'fp-ts/lib/IO';
import { server } from './http.server';
import { microservice } from './microservice/server/microservice.server';

const main: IO<void> = async () => {
  (await microservice)();
  await (await server)();
};

main();
