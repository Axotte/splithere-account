/* eslint-disable @typescript-eslint/no-explicit-any */
import { MessagingClient } from '@marblejs/messaging';
import { AuthenticationEventType } from '../events/authentication.event';
import { Event, createReader } from '@marblejs/core';
import { VerifyTokenDto } from '../validators/messages/verifyToken.dto';
import { of } from 'rxjs';
import { Mock } from 'moq.ts';

const MockedAuthClient = new Mock<MessagingClient>()
  .setup((instance) => instance.send)
  .returns((event): any => {
    event.type = AuthenticationEventType.TOKEN_VALID;
    event.payload = {
      uid: (event.payload as VerifyTokenDto).token,
    };
    return of(event);
  });

export const mockedSendFail = jest.fn().mockImplementation(
  async (): Promise<Event> => ({
    type: AuthenticationEventType.INVALID_TOKEN,
    payload: {
      message: 'Fail',
    },
  })
);

export const MockedAuthClientReader = createReader(() =>
  MockedAuthClient.object()
);
