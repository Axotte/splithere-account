import { Sequelize } from 'sequelize-typescript';
import {
  createContextToken,
  createReader,
  useContext,
  LoggerToken,
  LoggerTag,
  LoggerLevel,
} from '@marblejs/core';

export const sequelize = new Sequelize({
  database: 'splithere-db',
  username: 'user',
  password: 'test123',
  dialect: 'sqlite',
  storage: ':memory:',
  models: [`${__dirname}/**/*.model.ts`],
  modelMatch: (filename, member) => {
    return (
      filename.substring(0, filename.indexOf('.model')) === member.toLowerCase()
    );
  },
});

const database = sequelize.sync();

export const SequelizeToken = createContextToken<Sequelize>('Sequelize');

export const SequelizeReader = createReader(async (ask) => {
  const logger = useContext(LoggerToken)(ask);
  try {
    return await database;
  } catch (e) {
    logger({
      tag: LoggerTag.CORE,
      level: LoggerLevel.ERROR,
      type: 'Database',
      message: `Failed to sync db: ${e}`,
    })();
  }
});
