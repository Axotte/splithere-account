import {
  Model,
  Table,
  ForeignKey,
  Column,
  DataType,
  CreatedAt,
  UpdatedAt,
  Default,
  PrimaryKey,
  AutoIncrement,
} from 'sequelize-typescript';
import { User } from './user.model';

export enum FriendshipStatus {
  REQUESTED = 'REQUESTED',
  ACCEPTED = 'ACCEPTED',
}

@Table
export class Friendship extends Model<Friendship> {
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id: number;

  @ForeignKey(() => User)
  @Column({
    unique: false,
    type: DataType.UUIDV4,
  })
  friendshipRecieverUid: string;

  @ForeignKey(() => User)
  @Column({
    unique: false,
    type: DataType.UUIDV4,
  })
  friendshipSenderUid: string;

  @Default('REQUESTED')
  @Column(DataType.ENUM('REQUESTED', 'ACCEPTED'))
  status: 'REQUESTED' | 'ACCEPTED';

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;
}
