import {
  Table,
  Column,
  Model,
  Unique,
  PrimaryKey,
  DataType,
  Default,
  CreatedAt,
  UpdatedAt,
  BelongsToMany,
  DefaultScope,
  Scopes,
} from 'sequelize-typescript';
import { UUIDV4 } from 'sequelize';
import { Friendship, FriendshipStatus } from './friendship.model';

@DefaultScope(() => ({
  attributes: ['uid', 'email', 'firstName', 'surname'],
}))
@Scopes(() => ({
  withFriends: {
    include: [
      {
        model: User,
        as: 'friendshipRecievers',
        through: {
          attributes: [],
          where: {
            status: FriendshipStatus.ACCEPTED,
          },
        },
      },
      {
        model: User,
        as: 'friendshipSenders',
        through: {
          attributes: [],
          where: {
            status: FriendshipStatus.ACCEPTED,
          },
        },
      },
    ],
  },
  withFreindshipRequests: {
    include: [
      {
        model: User,
        as: 'friendshipSenders',
        through: {
          attributes: [],
          where: {
            status: FriendshipStatus.REQUESTED,
          },
        },
      },
    ],
  },
}))
@Table
export class User extends Model<User> {
  @Unique
  @PrimaryKey
  @Default(UUIDV4)
  @Column(DataType.UUIDV4)
  uid: string;

  @Unique
  @Column(DataType.STRING)
  email: string;

  @Column(DataType.STRING)
  firstName: string;

  @Column(DataType.STRING)
  surname: string;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @BelongsToMany(() => User, {
    foreignKey: 'friendshipSenderUid',
    otherKey: 'friendshipRecieverUid',
    uniqueKey: 'id',
    through: {
      model: () => Friendship,
      unique: false,
    },
  })
  friendshipRecievers: User[];

  @BelongsToMany(() => User, {
    foreignKey: 'friendshipRecieverUid',
    otherKey: 'friendshipSenderUid',
    uniqueKey: 'id',
    through: {
      model: () => Friendship,
      unique: false,
    },
  })
  friendshipSenders: User[];

  getFriends = (): User[] => [
    ...this.friendshipRecievers,
    ...this.friendshipSenders,
  ];
}
