import { createHttpTestBed, createTestBedSetup } from '@marblejs/testing';
import { listener } from './api/http.listener';
import { bindEagerlyTo, bindTo } from '@marblejs/core';
import { SequelizeToken, SequelizeReader } from './db';
import { AuthClientToken } from './microservice/client/auth.client';
import { UserServiceToken, UserServiceReader } from './services/user.service';
import {
  FriendsServiceToken,
  FriendsServiceReader,
} from './services/friends.service';
import { MockedAuthClientReader } from './__mocks__/auth.client';
const testBed = createHttpTestBed({
  listener,
});

export const useTextBedSetup = createTestBedSetup({
  testBed,
  dependencies: [
    bindEagerlyTo(SequelizeToken)(SequelizeReader),
    bindEagerlyTo(AuthClientToken)(MockedAuthClientReader),
    bindTo(UserServiceToken)(UserServiceReader),
    bindTo(FriendsServiceToken)(FriendsServiceReader),
  ],
});
