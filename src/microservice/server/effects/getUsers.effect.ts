import { MsgEffect, reply } from '@marblejs/messaging';
import { matchEvent, act, useContext } from '@marblejs/core';
import { GetUsersEvent, UserEventType } from '../../../events/user.event';
import { of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { GetUsersDto } from '../../../validators/messages/getUsers.dto';
import { UserServiceToken } from '../../../services/user.service';

export const getUsers$: MsgEffect = (event$, ctx) => {
  const usersService = useContext(UserServiceToken)(ctx.ask);
  return event$.pipe(
    matchEvent(GetUsersEvent),
    act((event) =>
      of(event).pipe(
        map((event) => {
          console.log(event);
          return (event.payload as GetUsersDto).uids;
        }),
        mergeMap(usersService.getUsers),
        map((users) => ({
          type: UserEventType.USERS_RESPONSE,
          payload: {
            users,
          },
        })),
        map(reply(event))
      )
    )
  );
};
