import { MsgEffect, reply } from '@marblejs/messaging';
import { matchEvent, act, useContext } from '@marblejs/core';
import { of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { AuthMessengingServiceToken } from '../../../services/authMessenging.service';
import { CreateUserEvent } from '../../../events/user.event';

export const createUser$: MsgEffect = (event$, ctx) => {
  const authMessengingService = useContext(AuthMessengingServiceToken)(ctx.ask);
  return event$.pipe(
    matchEvent(CreateUserEvent),
    act((event) => {
      return of(event).pipe(
        map((event) => event.payload),
        mergeMap(authMessengingService.createUser$),
        map(reply(event))
      );
    })
  );
};
