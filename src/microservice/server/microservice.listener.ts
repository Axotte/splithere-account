import { messagingListener } from '@marblejs/messaging';
import { createUser$ } from './effects/createUser.effect';
import { getUsers$ } from './effects/getUsers.effect';

export const microserviceListener = messagingListener({
  effects: [createUser$, getUsers$],
});
