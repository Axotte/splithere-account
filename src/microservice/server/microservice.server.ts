import { createMicroservice, Transport } from '@marblejs/messaging';
import { microserviceListener } from './microservice.listener';
import { SequelizeToken, SequelizeReader } from '../../db';
import { bindEagerlyTo, bindTo } from '@marblejs/core';
import {
  AuthMessengingServiceToken,
  AuthMessengingServiceReader,
} from '../../services/authMessenging.service';
import {
  UserServiceToken,
  UserServiceReader,
} from '../../services/user.service';

export const microservice = createMicroservice({
  transport: Transport.AMQP,
  options: {
    host: 'amqp://localhost:5672',
    queue: 'account_queue',
  },
  listener: microserviceListener,
  dependencies: [
    bindEagerlyTo(SequelizeToken)(SequelizeReader),
    bindTo(AuthMessengingServiceToken)(AuthMessengingServiceReader),
    bindTo(UserServiceToken)(UserServiceReader),
  ],
});
