import { createContextToken } from '@marblejs/core';
import {
  messagingClient,
  MessagingClient,
  Transport,
} from '@marblejs/messaging';

export const authClient = messagingClient({
  transport: Transport.AMQP,
  options: {
    host: 'amqp://localhost:5672',
    queue: 'auth_queue',
  },
});

export const AuthClientToken = createContextToken<MessagingClient>(
  'AuthClientToken'
);
