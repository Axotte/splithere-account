import { t, requestValidator$ } from '@marblejs/middleware-io';

export const FriendshipDto = t.type({
  friendUid: t.string,
});

export type FriendshipDto = t.TypeOf<typeof FriendshipDto>;

export const validateFriendshipModel$ = requestValidator$({
  body: FriendshipDto,
});
