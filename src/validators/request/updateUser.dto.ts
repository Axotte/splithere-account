import { t } from '@marblejs/middleware-io';

export const UpdateUserDto = t.type({
  firstName: t.string,
  surname: t.string,
});

export type UpdateUserDto = t.TypeOf<typeof UpdateUserDto>;
