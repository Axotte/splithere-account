import { t } from '@marblejs/middleware-io';

export const CreateUserDto = t.type({
  email: t.string,
});

export type CreateUserDto = t.TypeOf<typeof CreateUserDto>;
