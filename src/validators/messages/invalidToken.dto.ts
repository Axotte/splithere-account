import { t } from '@marblejs/middleware-io';

export const InvalidTokenDto = t.type({
  message: t.string,
});

export type InvalidTokenDto = t.TypeOf<typeof InvalidTokenDto>;
