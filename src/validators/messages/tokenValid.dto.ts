import { t } from '@marblejs/middleware-io';

export const TokenValidDto = t.type({
  uid: t.string,
});

export type TokenValidDto = t.TypeOf<typeof TokenValidDto>;
