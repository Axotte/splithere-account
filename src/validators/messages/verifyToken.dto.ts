import { t } from '@marblejs/middleware-io';
import { optional } from '../../utils/optional';

export const VerifyTokenDto = t.type({
  token: optional(t.string),
});

export type VerifyTokenDto = t.TypeOf<typeof VerifyTokenDto>;
