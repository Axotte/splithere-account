import { t } from '@marblejs/middleware-io';

export const GetUsersDto = t.type({
  uids: t.array(t.string),
});

export type GetUsersDto = t.TypeOf<typeof GetUsersDto>;
