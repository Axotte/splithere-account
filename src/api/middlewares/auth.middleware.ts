import {
  HttpMiddlewareEffect,
  useContext,
  Event,
  HttpError,
  HttpStatus,
} from '@marblejs/core';
import { AuthClientToken } from '../../microservice/client/auth.client';
import { tap, map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import {
  getVerifyTokenEvent,
  AuthenticationEventType,
} from '../../events/authentication.event';
import { InvalidTokenDto } from '../../validators/messages/invalidToken.dto';
import { TokenValidDto } from '../../validators/messages/tokenValid.dto';

export const authenticate$: HttpMiddlewareEffect = (req$, ctx) => {
  const authClient = useContext(AuthClientToken)(ctx.ask);
  return req$.pipe(
    mergeMap((req) =>
      of(req).pipe(
        map((req) => req.headers.authorization),
        tap(console.log),
        mergeMap((token) =>
          authClient.send(getVerifyTokenEvent({ token: token?.toString() }))
        ),
        map((response) => response as Event),
        tap((event) => {
          if (event.type === AuthenticationEventType.INVALID_TOKEN) {
            throw new HttpError(
              (event.payload as InvalidTokenDto).message,
              HttpStatus.UNAUTHORIZED
            );
          }
        }),
        map((event) => {
          req.meta.userUid = (event.payload as TokenValidDto).uid;
          return req;
        })
      )
    )
  );
};
