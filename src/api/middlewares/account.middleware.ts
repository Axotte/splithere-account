import {
  HttpMiddlewareEffect,
  useContext,
  HttpError,
  HttpStatus,
} from '@marblejs/core';
import { mergeMap } from 'rxjs/operators';
import { UserServiceToken } from '../../services/user.service';

export const getUser$: HttpMiddlewareEffect = (req$, ctx) => {
  const userService = useContext(UserServiceToken)(ctx.ask);
  return req$.pipe(
    mergeMap(async (req) => {
      const userUid = req.meta.userUid;
      const user = await userService.getUserByUid(userUid);
      console.log(`user: ${user.email}`);
      if (!user)
        throw new HttpError(
          'No user found for this token',
          HttpStatus.INTERNAL_SERVER_ERROR
        );
      req.meta.user = user;
      return req;
    })
  );
};
