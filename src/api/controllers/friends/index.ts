import { combineRoutes } from '@marblejs/core';
import { acceptFriendship$ } from './acceptFriendship.effect';
import { denyFriendship$ } from './denyFriendship.effect';
import { getAllFriends$ } from './getAllFriends.effect';
import { getPendingRequests$ } from './getPendingRequests.effect';
import { requestFriendship$ } from './requestFriendship.effect';

export const friends$ = combineRoutes('/friends', {
  effects: [
    acceptFriendship$,
    denyFriendship$,
    getAllFriends$,
    getPendingRequests$,
    requestFriendship$,
  ],
});
