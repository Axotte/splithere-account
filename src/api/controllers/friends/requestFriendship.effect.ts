import { r, useContext, HttpStatus } from '@marblejs/core';
import {
  validateFriendshipModel$,
  FriendshipDto,
} from '../../../validators/request/friendship.dto';
import { mergeMap, mapTo } from 'rxjs/operators';
import { FriendsServiceToken } from '../../../services/friends.service';

export const requestFriendship$ = r.pipe(
  r.matchPath('/request'),
  r.matchType('POST'),
  r.use(validateFriendshipModel$),
  r.useEffect((req$, ctx) => {
    const friendsService = useContext(FriendsServiceToken)(ctx.ask);
    return req$.pipe(
      mergeMap((req) => {
        const friendUid = (req.body as FriendshipDto).friendUid;
        return friendsService.requestFriendship$(req.meta.userUid, friendUid);
      }),
      mapTo({ status: HttpStatus.NO_CONTENT })
    );
  })
);
