import { r, useContext } from '@marblejs/core';
import { mergeMap, map } from 'rxjs/operators';
import { FriendsServiceToken } from '../../../services/friends.service';

export const getPendingRequests$ = r.pipe(
  r.matchPath('/pending'),
  r.matchType('GET'),
  r.useEffect((req$, ctx) => {
    const friendsService = useContext(FriendsServiceToken)(ctx.ask);
    return req$.pipe(
      mergeMap((req) => {
        return friendsService.getFriendsRequests$(req.meta.userUid);
      }),
      map((body) => ({ body }))
    );
  })
);
