import { r, useContext } from '@marblejs/core';
import { mergeMap, map } from 'rxjs/operators';
import { FriendsServiceToken } from '../../../services/friends.service';

export const getAllFriends$ = r.pipe(
  r.matchPath('/'),
  r.matchType('GET'),
  r.useEffect((req$, ctx) => {
    const friendsService = useContext(FriendsServiceToken)(ctx.ask);
    return req$.pipe(
      mergeMap((req) => {
        return friendsService.getAllFriends$(req.meta.userUid);
      }),
      map((body) => ({ body }))
    );
  })
);
