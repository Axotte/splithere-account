import { combineRoutes } from '@marblejs/core';
import { getCurrentUser$ } from './getCurrentUser.effect';
import { updateCurrentUser$ } from './updateCurrentUser.effect';

export const user$ = combineRoutes('/user', {
  effects: [getCurrentUser$, updateCurrentUser$],
});
