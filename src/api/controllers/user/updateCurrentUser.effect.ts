import { r, useContext } from '@marblejs/core';
import { requestValidator$ } from '@marblejs/middleware-io';
import { UpdateUserDto } from '../../../validators/request/updateUser.dto';
import { UserServiceToken } from '../../../services/user.service';
import { mergeMap, map } from 'rxjs/operators';
import { of } from 'rxjs';

const validateUpdateUserModel$ = requestValidator$({
  body: UpdateUserDto,
});

export const updateCurrentUser$ = r.pipe(
  r.matchPath('/'),
  r.matchType('PATCH'),
  r.use(validateUpdateUserModel$),
  r.useEffect((req$, ctx) => {
    const userService = useContext(UserServiceToken)(ctx.ask);
    return req$.pipe(
      mergeMap((req) =>
        of(req.body as UpdateUserDto).pipe(
          mergeMap((user) => userService.updateUser$(req.meta.userUid, user))
        )
      ),
      map((body) => ({
        body,
      }))
    );
  })
);
