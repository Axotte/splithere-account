import { r } from '@marblejs/core';
import { map } from 'rxjs/operators';

export const getCurrentUser$ = r.pipe(
  r.matchPath('/'),
  r.matchType('GET'),
  r.useEffect((req$) => req$.pipe(map((req) => ({ body: req.meta.user }))))
);
