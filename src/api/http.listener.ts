import { httpListener } from '@marblejs/core';
import { user$ } from './controllers/user';
import { friends$ } from './controllers/friends';
import { bodyParser$ } from '@marblejs/middleware-body';
import { logger$ } from '@marblejs/middleware-logger';
import { authenticate$ } from './middlewares/auth.middleware';
import { getUser$ } from './middlewares/account.middleware';

export const listener = httpListener({
  middlewares: [bodyParser$(), logger$(), authenticate$, getUser$],
  effects: [user$, friends$],
});
