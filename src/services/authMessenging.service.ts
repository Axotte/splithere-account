import { User } from '../db/models/user.model';
import { CreateUserDto } from '../validators/request/createUser.dto';
import { Observable, of } from 'rxjs';
import {
  Event,
  LoggerTag,
  LoggerLevel,
  createContextToken,
  createReader,
  useContext,
  LoggerToken,
  Logger,
} from '@marblejs/core';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { UserEventType } from '../events/user.event';

export class AuthMessengingService {
  logger: Logger;

  constructor(logger: Logger) {
    this.logger = logger;
  }

  createUser$ = (createUserModel: CreateUserDto): Observable<Event> => {
    return of(createUserModel).pipe(
      mergeMap(async (user) => {
        const createdUser = await User.create(user);
        return {
          uid: createdUser.uid,
          email: createdUser.email,
        };
      }),
      map((user) => ({ type: UserEventType.USER_CREATED, payload: user })),
      catchError((error) => {
        this.logger({
          tag: LoggerTag.MESSAGING,
          level: LoggerLevel.INFO,
          type: 'Create user',
          message: `Failed to create user ${error}`,
        })();
        return of({
          type: UserEventType.USER_CREATION_ERROR,
          payload: {
            message: 'Failed to create user',
          },
        });
      })
    );
  };
}

export const AuthMessengingServiceToken = createContextToken<
  AuthMessengingService
>('AuthMessengingServiceService');

export const AuthMessengingServiceReader = createReader((ask) => {
  const logger = useContext(LoggerToken)(ask);
  return new AuthMessengingService(logger);
});
