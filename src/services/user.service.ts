import { User } from '../db/models/user.model';
import {
  createContextToken,
  createReader,
  HttpError,
  HttpStatus,
} from '@marblejs/core';
import { Observable, of } from 'rxjs';
import { UpdateUserDto } from '../validators/request/updateUser.dto';
import { mergeMap, tap } from 'rxjs/operators';
import { Op } from 'sequelize';

export class UserService {
  getUserByUid = (uid: string): Promise<User> => {
    return User.findOne({
      where: {
        uid,
      },
    });
  };

  getUsers = (uids: string[]): Promise<User[]> => {
    return User.findAll({
      where: {
        uid: {
          [Op.or]: uids,
        },
      },
    });
  };

  updateUser$ = (
    uid: string,
    updateUserDto: UpdateUserDto
  ): Observable<User> => {
    return of(uid).pipe(
      mergeMap((uid) => User.update(updateUserDto, { where: { uid } })),
      tap((result) => {
        if (result[0] !== 1)
          throw new HttpError(
            'Failed to update user',
            HttpStatus.INTERNAL_SERVER_ERROR
          );
      }),
      mergeMap(() => this.getUserByUid(uid))
    );
  };
}

export const UserServiceToken = createContextToken<UserService>(
  'UserServiceToken'
);

export const UserServiceReader = createReader(() => new UserService());
