import {
  createContextToken,
  createReader,
  HttpError,
  HttpStatus,
} from '@marblejs/core';
import { Observable, of } from 'rxjs';
import { tap, mergeMap, mapTo, switchMap, map } from 'rxjs/operators';
import { User } from '../db/models/user.model';
import { Friendship, FriendshipStatus } from '../db/models/friendship.model';
import { Op } from 'sequelize';

export class FriendsService {
  requestFriendship$ = (
    currentUserUid: string,
    friendUid: string
  ): Observable<void> =>
    of(null).pipe(
      tap(() => {
        if (currentUserUid === friendUid)
          throw new HttpError(
            'You cannot be friend with yourself',
            HttpStatus.BAD_REQUEST
          );
      }),
      mergeMap(() => User.findOne({ where: { uid: friendUid } })),
      tap((friend) => {
        if (!friend)
          throw new HttpError(
            'There is no user with this uid',
            HttpStatus.BAD_REQUEST
          );
      }),
      mergeMap(() =>
        Friendship.findOne({
          where: {
            friendshipRecieverUid: {
              [Op.or]: [friendUid, currentUserUid],
            },
            friendshipSenderUid: {
              [Op.or]: [friendUid, currentUserUid],
            },
          },
        })
      ),
      tap((friendship) => {
        if (friendship)
          throw new HttpError('Request already exist', HttpStatus.BAD_REQUEST);
      }),
      mergeMap(() =>
        new Friendship({
          friendshipRecieverUid: friendUid,
          friendshipSenderUid: currentUserUid,
        }).save()
      ),
      mapTo(null)
    );

  acceptFriendshipRequest$ = (
    currentUserUid: string,
    friendUid: string
  ): Observable<void> => {
    return of(null).pipe(
      mergeMap(() =>
        Friendship.findOne({
          where: {
            friendshipRecieverUid: currentUserUid,
            friendshipSenderUid: friendUid,
          },
        })
      ),
      tap((friendship) => {
        if (!friendship)
          throw new HttpError('There are no such request', HttpStatus.CONFLICT);
        if (friendship.status === FriendshipStatus.ACCEPTED)
          throw new HttpError('Request already accepted', HttpStatus.CONFLICT);
        if (friendship.friendshipRecieverUid !== currentUserUid)
          throw new HttpError(
            'You cannot accept this request',
            HttpStatus.CONFLICT
          );
      }),
      mergeMap((friendship) =>
        friendship.update({ status: FriendshipStatus.ACCEPTED })
      ),
      mapTo(null)
    );
  };

  denyFriendshipRequest$ = (
    currentUserUid: string,
    friendUid: string
  ): Observable<void> => {
    return of(null).pipe(
      mergeMap(() =>
        Friendship.findOne({
          where: {
            friendshipRecieverUid: {
              [Op.or]: [friendUid, currentUserUid],
            },
            friendshipSenderUid: {
              [Op.or]: [friendUid, currentUserUid],
            },
          },
        })
      ),
      tap((friendship) => {
        if (!friendship)
          throw new HttpError('There are no such request', HttpStatus.CONFLICT);
      }),
      mergeMap((friendship) => friendship.destroy())
    );
  };

  getAllFriends$ = (currentUserUid: string): Observable<User[]> => {
    return of(null).pipe(
      switchMap(() => {
        return User.scope('withFriends').findOne({
          where: {
            uid: currentUserUid,
          },
        });
      }),
      map((user) => user.getFriends())
    );
  };

  getFriendsRequests$ = (currentUserUid: string): Observable<User[]> => {
    return of(null).pipe(
      switchMap(() => {
        return User.scope('withFreindshipRequests').findOne({
          where: {
            uid: currentUserUid,
          },
        });
      }),
      map((user) => user.friendshipSenders)
    );
  };
}

export const FriendsServiceToken = createContextToken<FriendsService>(
  'FriendsServiceToken'
);

export const FriendsServiceReader = createReader(() => new FriendsService());
