import { Event } from '@marblejs/core';
import { VerifyTokenDto } from '../validators/messages/verifyToken.dto';

export enum AuthenticationEventType {
  VERIFY_TOKEN = 'VERIFY_TOKEN',
  TOKEN_VALID = 'TOKEN_VALID',
  INVALID_TOKEN = 'INVALID_TOKEN',
}

export const getVerifyTokenEvent = (payload: VerifyTokenDto): Event => ({
  type: AuthenticationEventType.VERIFY_TOKEN,
  payload: payload,
});
