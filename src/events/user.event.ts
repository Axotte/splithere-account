import { event } from '@marblejs/core';
import { CreateUserDto } from '../validators/request/createUser.dto';
import { GetUsersDto } from '../validators/messages/getUsers.dto';

export enum UserEventType {
  USER_CREATED = 'USER_CREATED',
  CREATE_USER = 'CREATE_USER',
  USER_CREATION_ERROR = 'USER_CREATION_ERROR',
  GET_USERS = 'GET_USERS',
  USERS_RESPONSE = 'USERS_RESPONSE',
}

export const CreateUserEvent = event(UserEventType.CREATE_USER)(CreateUserDto);

export const GetUsersEvent = event(UserEventType.GET_USERS)(GetUsersDto);
