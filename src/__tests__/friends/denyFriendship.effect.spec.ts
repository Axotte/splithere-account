import { useTextBedSetup } from '../../test.setup';
import { Friendship, FriendshipStatus } from '../../db/models/friendship.model';
import { Op } from 'sequelize';
import { User } from '../../db/models/user.model';
import { pipe } from 'fp-ts/lib/function';
import { HttpStatus } from '@marblejs/core';

describe('denyFriendship$', () => {
  const testUserUid = '30abec31-0c98-45a9-b411-8321aa31b444';
  const secondTestUserUid = '30abec31-0c98-45a9-b411-8321aa31b445';
  const testBedSetup = useTextBedSetup();

  beforeEach(async () => {
    const { finish } = await testBedSetup.useTestBed();
    await Friendship.destroy({
      where: {
        friendshipRecieverUid: {
          [Op.or]: [testUserUid, secondTestUserUid],
        },
        friendshipSenderUid: {
          [Op.or]: [testUserUid, secondTestUserUid],
        },
      },
    });
    await User.destroy({ where: { uid: testUserUid } });
    await User.destroy({ where: { uid: secondTestUserUid } });

    await new User({
      uid: testUserUid,
      firstName: 'test',
      surname: 'test',
      email: 'test@test.com',
    }).save();

    await new User({
      uid: secondTestUserUid,
      firstName: 'test2',
      surname: 'test2',
      email: 'test2@test.com',
    }).save();

    await new Friendship({
      friendshipRecieverUid: testUserUid,
      friendshipSenderUid: secondTestUserUid,
    }).save();

    await finish();
  });

  test('POST "/friends/deny" success - not accepted friendship', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('POST'),
      request.withPath('/friends/deny'),
      request.withHeaders({
        Authorization: secondTestUserUid,
      }),
      request.withBody({
        friendUid: testUserUid,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.NO_CONTENT);

    await finish();
  });

  test('POST "/friends/deny" success - accepted friendship', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const frienship = await Friendship.findOne({
      where: {
        friendshipRecieverUid: testUserUid,
        friendshipSenderUid: secondTestUserUid,
      },
    });

    frienship.status = FriendshipStatus.ACCEPTED;
    await frienship.save();

    const response = await pipe(
      request('POST'),
      request.withPath('/friends/deny'),
      request.withHeaders({
        Authorization: secondTestUserUid,
      }),
      request.withBody({
        friendUid: testUserUid,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.NO_CONTENT);

    await finish();
  });

  test('POST "/friends/deny" failed - friendship dont exist', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('POST'),
      request.withPath('/friends/deny'),
      request.withHeaders({
        Authorization: secondTestUserUid,
      }),
      request.withBody({
        friendUid: '30abec31-0c98-45a9-b411-8321aa31b4499',
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.CONFLICT);
    expect(response.body.error.message).toEqual('There are no such request');

    await finish();
  });
});
