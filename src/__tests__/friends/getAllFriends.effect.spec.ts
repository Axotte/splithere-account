import { useTextBedSetup } from '../../test.setup';
import { User } from '../../db/models/user.model';
import { Friendship, FriendshipStatus } from '../../db/models/friendship.model';
import { pipe } from 'fp-ts/lib/function';
import { HttpStatus } from '@marblejs/core';

describe('getAllFriends$', () => {
  const testUserUid = '50abec31-0c98-45a9-b411-8321aa31b777';
  const secondTestUserUid = '50abec31-0c98-45a9-b411-8321aa31b778';
  const thirdTestUserUid = '50abec31-0c98-45a9-b411-8321aa31b779';
  const fourthTestUserUid = '50abec31-0c98-45a9-b411-8321aa31b780';
  const testBedSetup = useTextBedSetup();

  beforeAll(async () => {
    const { finish } = await testBedSetup.useTestBed();

    await new User({
      uid: testUserUid,
      firstName: 'testUser1',
      surname: 'testUser1',
      email: 'testUser1@test.com',
    }).save();

    await new User({
      uid: secondTestUserUid,
      firstName: 'testUser2',
      surname: 'testUser2',
      email: 'testUser2@test.com',
    }).save();

    await new User({
      uid: thirdTestUserUid,
      firstName: 'testUser3',
      surname: 'testUser3',
      email: 'testUser3@test.com',
    }).save();

    await new User({
      uid: fourthTestUserUid,
      firstName: 'testUser4',
      surname: 'testUser4',
      email: 'testUser4@test.com',
    }).save();

    await new Friendship({
      friendshipRecieverUid: testUserUid,
      friendshipSenderUid: secondTestUserUid,
      status: FriendshipStatus.ACCEPTED,
    }).save();

    await new Friendship({
      friendshipRecieverUid: thirdTestUserUid,
      friendshipSenderUid: testUserUid,
    }).save();

    await new Friendship({
      friendshipRecieverUid: thirdTestUserUid,
      friendshipSenderUid: secondTestUserUid,
    }).save();

    await new Friendship({
      friendshipRecieverUid: testUserUid,
      friendshipSenderUid: fourthTestUserUid,
      status: FriendshipStatus.ACCEPTED,
    }).save();

    await finish();
  });

  test('GET "/friends" success - zero result', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('GET'),
      request.withPath('/friends'),
      request.withHeaders({
        Authorization: thirdTestUserUid,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.OK);
    expect(response.body).toHaveLength(0);

    await finish();
  });

  test('GET "/friends" success - one result', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('GET'),
      request.withPath('/friends'),
      request.withHeaders({
        Authorization: fourthTestUserUid,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.OK);
    expect(response.body).toHaveLength(1);

    await finish();
  });

  test('GET "/friends" success - two results', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('GET'),
      request.withPath('/friends'),
      request.withHeaders({
        Authorization: testUserUid,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.OK);
    expect(response.body).toHaveLength(2);

    await finish();
  });
});
