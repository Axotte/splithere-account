import { pipe } from 'fp-ts/lib/function';
import { User } from '../../db/models/user.model';
import { useTextBedSetup } from '../../test.setup';
import { HttpStatus } from '@marblejs/core';

describe('requestFriendship$', () => {
  const testUserUid = '30abec31-0c98-45a9-b411-8321aa31b236';
  const secondTestUserUid = '30abec31-0c98-45a9-b411-8321aa31b237';
  const testBedSetup = useTextBedSetup();

  beforeAll(async () => {
    const { finish, request } = await testBedSetup.useTestBed();

    await new User({
      uid: testUserUid,
      firstName: 'testUser1',
      surname: 'testUser1',
      email: 'testUser1@test.com',
    }).save();

    await new User({
      uid: secondTestUserUid,
      firstName: 'testUser2',
      surname: 'testUser2',
      email: 'testUser2@test.com',
    }).save();

    await pipe(
      request('POST'),
      request.withPath('/friends/request'),
      request.withHeaders({
        Authorization: testUserUid,
      }),
      request.withBody({
        friendUid: secondTestUserUid,
      }),
      request.send
    );

    await finish();
  });

  test('POST "/friends/accept" success', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('POST'),
      request.withPath('/friends/accept'),
      request.withHeaders({
        Authorization: secondTestUserUid,
      }),
      request.withBody({
        friendUid: testUserUid,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.NO_CONTENT);

    await finish();
  });

  test('POST "/friends/accept" failed - no such request', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('POST'),
      request.withPath('/friends/accept'),
      request.withHeaders({
        Authorization: secondTestUserUid,
      }),
      request.withBody({
        friendUid: '30abec31-0c98-45a9-b411-8321aa31b111',
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.CONFLICT);
    expect(response.body.error.message).toEqual('There are no such request');

    await finish();
  });

  test('POST "/friends/accept" failed - request already accepted', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('POST'),
      request.withPath('/friends/accept'),
      request.withHeaders({
        Authorization: secondTestUserUid,
      }),
      request.withBody({
        friendUid: testUserUid,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.CONFLICT);
    expect(response.body.error.message).toEqual('Request already accepted');

    await finish();
  });

  test('POST "/friends/accept" failed - request already accepted', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('POST'),
      request.withPath('/friends/accept'),
      request.withHeaders({
        Authorization: secondTestUserUid,
      }),
      request.withBody({
        friendUid: testUserUid,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.CONFLICT);
    expect(response.body.error.message).toEqual('Request already accepted');

    await finish();
  });
});
