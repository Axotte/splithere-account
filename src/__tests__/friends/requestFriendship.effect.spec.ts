import { useTextBedSetup } from '../../test.setup';
import { User } from '../../db/models/user.model';
import { pipe } from 'fp-ts/lib/function';
import { HttpStatus } from '@marblejs/core';

describe('requestFriendship$', () => {
  const testUserUid = '30abec31-0c98-45a9-b411-8321aa31b226';
  const secondTestUserUid = '30abec31-0c98-45a9-b411-8321aa31b227';
  const testBedSetup = useTextBedSetup();

  beforeAll(async () => {
    const { finish } = await testBedSetup.useTestBed();

    await new User({
      uid: testUserUid,
      firstName: 'test',
      surname: 'test',
      email: 'test@test.com',
    }).save();

    await new User({
      uid: secondTestUserUid,
      firstName: 'test2',
      surname: 'test2',
      email: 'test2@test.com',
    }).save();

    await finish();
  });

  test('POST "/friends/request" success', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('POST'),
      request.withPath('/friends/request'),
      request.withHeaders({
        Authorization: testUserUid,
      }),
      request.withBody({
        friendUid: secondTestUserUid,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.NO_CONTENT);

    await finish();
  });

  test('POST "/friends/request" failed - invite yourself', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('POST'),
      request.withPath('/friends/request'),
      request.withHeaders({
        Authorization: testUserUid,
      }),
      request.withBody({
        friendUid: testUserUid,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    expect(response.body.error.message).toEqual(
      'You cannot be friend with yourself'
    );

    await finish();
  });

  test('POST "/friends/request" failed - friend request already exist', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('POST'),
      request.withPath('/friends/request'),
      request.withHeaders({
        Authorization: testUserUid,
      }),
      request.withBody({
        friendUid: secondTestUserUid,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
    expect(response.body.error.message).toEqual('Request already exist');

    await finish();
  });
});
