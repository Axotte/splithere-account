import { useTextBedSetup } from '../../test.setup';
import { User } from '../../db/models/user.model';
import { pipe } from 'fp-ts/lib/function';
import { HttpStatus } from '@marblejs/core';

describe('getCurrentUser$', () => {
  const testUserToken = '30abec31-0c98-45a9-b411-8321aa31b217';
  const testBedSetup = useTextBedSetup();

  beforeAll(async () => {
    const { finish } = await testBedSetup.useTestBed();

    await new User({
      uid: testUserToken,
      email: 'test@test.com',
    }).save();

    finish();
  });

  test('PATCH "/user" success', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('PATCH'),
      request.withPath('/user'),
      request.withHeaders({
        Authorization: testUserToken,
      }),
      request.withBody({
        firstName: 'Test',
        surname: 'Test',
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.OK);

    await finish();
  });

  test('PATCH "/user" fail - invalid model', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('PATCH'),
      request.withPath('/user'),
      request.withHeaders({
        Authorization: testUserToken,
      }),
      request.withBody({
        firstName: 'Test',
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);

    await finish();
  });
});
