import { useTextBedSetup } from '../../test.setup';
import { User } from '../../db/models/user.model';
import { pipe } from 'fp-ts/lib/function';
import { HttpStatus, useContext } from '@marblejs/core';
import { AuthClientToken } from '../../microservice/client/auth.client';
import { mockedSendFail } from '../../__mocks__/auth.client';

describe('getCurrentUser$', () => {
  const testUserToken = '30abec31-0c98-45a9-b411-8321aa31b216';
  const testBedSetup = useTextBedSetup();

  beforeAll(async () => {
    const { finish } = await testBedSetup.useTestBed();

    await new User({
      uid: testUserToken,
      firstName: 'test',
      surname: 'test',
      email: 'test@test.com',
    }).save();

    await finish();
  });

  test('GET "/user" success', async () => {
    const { request, finish } = await testBedSetup.useTestBed();

    const response = await pipe(
      request('GET'),
      request.withPath('/user'),
      request.withHeaders({
        Authorization: testUserToken,
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.OK);
    const body = response.body as User;
    expect(body.uid).toEqual(testUserToken);
    expect(body.email).toEqual('test@test.com');
    expect(body.surname).toEqual('test');
    expect(body.firstName).toEqual('test');
    expect(body.friendshipRecievers).toBeUndefined();
    expect(body.friendshipSenders).toBeUndefined();
    await finish();
  });

  test('GET "/user" fail - invalid token', async () => {
    const { request, finish, ask } = await testBedSetup.useTestBed();

    const mockedAuthClient = useContext(AuthClientToken)(ask);
    mockedAuthClient.send = mockedSendFail.bind(mockedAuthClient);

    const response = await pipe(
      request('GET'),
      request.withPath('/user'),
      request.withHeaders({
        Authorization: 'invalid token',
      }),
      request.send
    );

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
    await finish();
  });
});
